package com.hotelmgt.service;

import java.util.List;


import com.hotelmgt.model.User;

public interface UserService {
	
	String authenticateUser( String phoneNumber, String password);

	User registerUser(User user);

	
	List<User> findAllUser(User user);
	
	List<String> getColumns(String tblName);
}
