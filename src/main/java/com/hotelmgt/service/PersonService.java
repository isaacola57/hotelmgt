package com.hotelmgt.service;

import com.hotelmgt.model.Person;
import com.hotelmgt.model.User;

public interface PersonService {
	
	Person registerPerson(Person person);
	
}

