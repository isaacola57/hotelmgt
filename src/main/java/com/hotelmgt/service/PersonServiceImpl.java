package com.hotelmgt.service;

import java.security.CryptoPrimitive;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.hotelmgt.model.Person;
import com.hotelmgt.model.Role;
import com.hotelmgt.model.User;
import com.hotelmgt.repository.PersonRepository;
import com.hotelmgt.repository.UserRepository;

@Service
public class PersonServiceImpl implements PersonService{

	@Autowired
	private PersonRepository personRepository;
	
	@Autowired
	PasswordEncoder encode;
	


	@Override
	public Person registerPerson(Person person) {
		String fName = person.getfirstName();
		String lName = person.getlastName();
		String address = person.getAddress();
		String email = person.getEmail();
		
		
		person.setfirstName(fName);
		person.setlastName(lName);
		person.setAddress(address);
		person.setEmail(email);
		
		return personRepository.save(person);
		
	}


}
