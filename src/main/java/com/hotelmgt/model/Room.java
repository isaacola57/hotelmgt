package com.hotelmgt.model;

import javax.persistence.Entity;

@Entity(name = "room")
public class Room  extends AbstractProperty{

	public Room() {

	}
	
	public Room(String name, Double price, Boolean status, Lodge lodge) {
		this.name = name;
		this.price = price;
		this.status = status;
		this.lodge = lodge;
	}

	private static final long serialVersionUID = 1L;

	private String name;
	
	private Double price;
	
	private Boolean status;
	
	private Lodge lodge;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Lodge getLodge() {
		return lodge;
	}

	public void setLodge(Lodge lodge) {
		this.lodge = lodge;
	}

	@Override
	public String toString() {
		return "Room [name=" + name + ", price=" + price + ", status=" + status + ", lodge=" + lodge + "]";
	}

	public String getPhoneNumber() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getPassword() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setPhoneNumber(String phoneNoTrim) {
		// TODO Auto-generated method stub
		
	}

	public void setPassword(String encodedPassword) {
		// TODO Auto-generated method stub
		
	}

	
	
}
