package com.hotelmgt.model;

import javax.persistence.Entity;

@Entity(name = "transaction")
public class Transaction extends AbstractProperty {

	public Transaction() {

	}
	
	public Transaction(double amount, boolean confirmed) {
		this.amount = amount;
		this.confirmed = confirmed;
	}

	private static final long serialVersionUID = 1L;

	private double amount;
	
	private boolean confirmed = false;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public boolean isConfirmed() {
		return confirmed;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}

	@Override
	public String toString() {
		return "Transaction [amount=" + amount + ", confirmed=" + confirmed + "]";
	}
	
	

}
