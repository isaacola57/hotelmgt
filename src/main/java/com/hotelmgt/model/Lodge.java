package com.hotelmgt.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity(name = "lodge")
public class Lodge extends AbstractProperty{
		
	public Lodge() {

	}
	
	public Lodge(Guest guest, Room room, Period period, Transaction transaction) {
//		this.guest = guest;
		this.room = room;
		this.period = period;
		this.transaction = transaction;
	}

	private static final long serialVersionUID = 1L;
	
//	private Guest guest;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id", unique=true)
	private Room room;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id", unique=true)
	private Period period;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id", unique=true)
	private Transaction transaction;

//	
//	public Guest getGuest() {
//		return guest;
//	}
//
//	public void setGuest(Guest guest) {
//		this.guest = guest;
//	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	@Override
	public String toString() {
		return "Lodge [room=" + room + ", period=" + period + ", transaction=" + transaction + "]";
	}


}