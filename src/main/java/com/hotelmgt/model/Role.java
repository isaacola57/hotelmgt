package com.hotelmgt.model;

import javax.persistence.Entity;

@Entity(name = "role")
public class Role  extends AbstractProperty {

	public Role() {
		
	}
	
	public Role(String manager, String receptionist) {
		this.manager = manager;
		this.receptionist = receptionist;
	}

	
	private static final long serialVersionUID = 1L;

	private String manager;
	
	private String receptionist;

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public String getReceptionist() {
		return receptionist;
	}

	public void setReceptionist(String receptionist) {
		this.receptionist = receptionist;
	}

	@Override
	public String toString() {
		return "Role [manager=" + manager + ", receptionist=" + receptionist + "]";
	}
	
	
}
