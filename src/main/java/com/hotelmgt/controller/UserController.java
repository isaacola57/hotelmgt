package com.hotelmgt.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;

import com.hotelmgt.model.Person;
import com.hotelmgt.model.User;
import com.hotelmgt.service.PersonService;
import com.hotelmgt.service.UserService;

@Controller
public class UserController {
	
	@Autowired
	private UserService userService;

	
	@Autowired
	private PersonService personService;
	
	@GetMapping(value="/")
    public String index() {
        return "login";
    }
	
	@GetMapping(value="/form")
    public String form() {
        return "form";
    }
	

	@GetMapping("/all")
	public String showAll(Model model) {
		String tableName = "user";

	    model.addAttribute("tableHead", userService.getColumns(tableName));
	    model.addAttribute("users", getUsersForTemplate());
	    System.out.println(userService.findAllUser(null));
        return "user";
        
       
    }
	
	@GetMapping(value="/signup")
    public String signUp(Model model) {
		model.addAttribute("user", new User());
        return "signup";
    }
	
	private List<List<Map<String, String>>> getUsersForTemplate(){
		List <List<Map<String, String>>> returnList = new ArrayList<>();
		List<User> users = userService.findAllUser(null);
		for(User user:users) {
//            DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  

			List<Map<String, String>> _users = new ArrayList<>();
			Map<String, String> userMap = new HashMap<>();
			userMap.put("id", user.getId().toString());
			userMap.put("phone_number", user.getPhoneNumber());
			userMap.put("password", user.getPassword());			
//			userMap.put("dateCreated", dateFormat.format(user.getDateCreated()));
//			userMap.put("dateModified", user.getDateModified().toString());
			
			_users.add(userMap);
			returnList.add(_users);
		}
		
		return returnList;
	}
	
	@PostMapping(value="/register")
    public String register(@ModelAttribute("user") User user) {
		System.out.println(user.toString());
		userService.registerUser(user);
		return "welcome";     
    }
	
	@PostMapping(value="/auth")
    public String auth(WebRequest webRequest) {
		String phoneNumber = webRequest.getParameter("phoneNumber");
		String password = webRequest.getParameter("password");
		return userService.authenticateUser(phoneNumber, password);     
    }
    
	@GetMapping(value="/profile")
    public String register(Model model) {
		model.addAttribute("person", new Person());
		return "UserProfile";   
    }
	
	@PostMapping(value="/profile")
    public String registered(@ModelAttribute("person") Person person) {
		personService.registerPerson(person);
		return "redirect:/";
		
		   
    }
}


